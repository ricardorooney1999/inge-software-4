SELECT
    c.relname AS table_name,
    a.attname AS column_name,
    t.typname AS data_type,
    a.attnotnull AS not_null,
    pg_get_expr(ad.adbin, ad.adrelid) AS default_value,
    con.contype AS constraint_type,
    con.conname AS constraint_name,
    pg_get_triggerdef(trg.oid) AS trigger_definition,
    pg_description.description AS table_comment
FROM
    pg_class c
JOIN
    pg_attribute a ON c.oid = a.attrelid
LEFT JOIN
    pg_type t ON a.atttypid = t.oid
LEFT JOIN
    pg_attrdef ad ON a.attrelid = ad.adrelid AND a.attnum = ad.adnum
LEFT JOIN
    pg_constraint con ON a.attrelid = con.conrelid AND a.attnum = ANY(con.conkey)
LEFT JOIN
    pg_trigger trg ON c.oid = trg.tgrelid
LEFT JOIN
    pg_description ON c.oid = pg_description.objoid AND pg_description.objsubid = 0
WHERE
    c.relname = 'usuarios'  -- Reemplaza 'usuarios' con el nombre de tu tabla
    AND a.attnum > 0
    AND NOT a.attisdropped
ORDER BY
    a.attnum;