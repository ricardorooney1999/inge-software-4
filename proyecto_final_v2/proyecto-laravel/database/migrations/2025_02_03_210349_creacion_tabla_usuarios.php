<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id('id_usuario');
            $table->string('nombre');
            $table->string('correo_electronico')->unique();
            $table->boolean('correo_electronico_verificado')->default(false);
            $table->string('password');
            $table->foreignId('id_rol')->constrained('roles', 'id_rol');
            $table->foreignId('id_area')->constrained('areas', 'id_area');
            $table->boolean('estado')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('usuarios');

    }
};
