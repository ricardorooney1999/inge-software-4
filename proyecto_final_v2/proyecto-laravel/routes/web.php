<?php

use App\Http\Controllers\CategoriaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\AuthController;
use App\Models\Usuario;
use App\Http\Controllers\UsuarioController;

// Autenticación
Route::get('/login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

// Rutas protegidas
Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('layouts.app');
    });
  
    // USUARIOS ADMIN
  Route::get('/usuarios', [UsuarioController::class, 'listarUsuarios'])->name('listarUsuarios');

    // PROVEEDORES ADMIN
  Route::get('/proveedores/create', [ProveedorController::class, 'cargarCategoriasParaProveedores'])->name('proveedores.create');
  Route::get('/proveedores', [ProveedorController::class, 'listarProveedores'])->name('listarProveedores');

    // CATEGORIAS ADMIN
  Route::get('/categorias/all', [CategoriaController::class, 'all'])->name('categorias.all');
  Route::get('/categorias/create', [CategoriaController::class, 'create'])->name('categorias.create');
  Route::post('/categorias/store', [CategoriaController::class, 'store'])->name('categorias.store');
  Route::put('/categorias/update/{id}', [CategoriaController::class, 'update'])->name('categorias.update');
  Route::delete('/categorias/delete/{id}', [CategoriaController::class, 'destroy'])->name('categorias.destroy');

});
// acceso para crear usuario admin por primera vez
Route::get('/create-admin', [Usuario::class, 'createAdminUser']);



// Route::get('/', function () {
//     return view('login');
// })->name('login');
// // Ruta para manejar el inicio de sesión y redirigir a la aplicación
// Route::post('/login', function () {
//     return redirect()->route('dashboard');
// })->name('login.submit');

// // Ruta protegida después de iniciar sesión
// Route::get('/dashboard', function () {
//     return view('layouts.app');
// })->name('dashboard');

// // Ruta para cerrar sesión

// // Se listan las tablas usuario, proveedores, categorias
// Route::get('/usuarios', [UsuariosLayoutsController::class, 'listarUsuarios'])->name('listarUsuarios');
// Route::get('/proveedores', [ProveedoresLayoutsController::class, 'listarProveedores'])->name('listarProveedores');
// Route::get('/categorias', action: [CategoriasLayoutsController::class, 'listarCategorias'])->name('listarCategorias');


// // mostrar formularios para agregar proveedores/categorias/usuarios
// Route::get('/proveedores/create', [ProveedoresLayoutsController::class, 'cargarCategoriasParaProveedores'])->name('proveedores.create');
// Route::get('/categorias/create', [CategoriasLayoutsController::class, 'cargarEstadosDeCategorias'])->name('categorias.create');
// Route::get('/usuarios/create', function () {
//     return view('layouts.usuarios.agregarUsuarios');
// })->name('usuarios.create');

// // enviar formularios
 