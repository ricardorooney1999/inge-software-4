<?php

namespace App\Http\Controllers;

use App\Models\Proveedor;
use App\Models\Categoria;

class ProveedorController extends Controller
{
    public function listarProveedores()
    {
        $proveedores = Proveedor::all();
        return view('main', [
            'content' => 'Gestión de Proveedores',
            'view' => 'layouts.proveedores.listarProveedores',
            'proveedores' => $proveedores
        ]);
    }

    public function cargarCategoriasParaProveedores()
    {
        $categorias = categoria::obtenerCategoriasActivas();
        return view('layouts.proveedores.agregarProveedores', compact('categorias'));
    }
    
}
