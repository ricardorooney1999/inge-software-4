<?php
namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoriaController extends Controller
{

    public function all()
    {
        $categorias = Categoria::all();
        return view('main', [
            'content' => 'Gestión de Categorias',
            'view' => 'layouts.categorias.listarCategorias',
            'categorias' => $categorias
        ]);
    }

    public function create()
    {
        return view('main', [
            'content' => 'Gestión de Categorias',
            'view' => 'layouts.categorias.crearCategorias',
        ]);
    }
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:255|unique:categorias,nombre',
        ], [
            'nombre.unique' => 'La categoría ya existe.',
        ]);
    
        try {
            Categoria::create([
                'nombre' => $request->nombre,
                'estado' => $request->estado ?? 0,
            ]);
    
            return redirect()->back()->with('success', 'Categoría creada exitosamente.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Error al crear la categoría.');
        }
    }
    public function update(Request $request, $id)
    {
        $categoria = Categoria::findOrFail($id);
    
        try {
            $categoria->update([
                'nombre' => $request->nombre,
                'estado' => $request->estado,
            ]);
    
            return redirect()->back()->with('success', 'Categoría actualizada correctamente.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Error al actualizar la categoría.');
        }
    }
    public function destroy($id)
    {
        $categoria = Categoria::findOrFail($id);
    
        try {
            $categoria->update(['estado' => 0]);
    
            return redirect()->back()->with('success', 'Categoría eliminada correctamente.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Error al eliminar la categoría.');
        }
    }
}