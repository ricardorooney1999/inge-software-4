<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Models\usuarios;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'correo_electronico' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt([
            'correo_electronico' => $credentials['correo_electronico'],
            'password' => $credentials['password'], // Laravel espera 'password' aquí
        ])) {
            $request->session()->regenerate();
            return redirect()->intended('/'); // Redirige al dashboard
        }

        return back()->with('auth_error', 'Las credenciales no coinciden con nuestros registros.');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }
}