<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function index()
    {
        $areas = Area::all(); // Obtiene todas las áreas
        return view('areas.index', compact('areas')); // Vista para listar áreas
    }


    public function store(Request $request)
    {
        // Validar los datos del formulario
        $request->validate([
            'nombre' => 'required|string|max:255',
            'descripcion' => 'nullable|string',
        ]);

        Area::create($request->all()); // Crea la nueva área
        return redirect()->route('areas.index')
                         ->with('success', 'Área creada con éxito.');
    }

    public function edit(Area $area)
    {
        return view('areas.edit', compact('area')); // Vista para editar área
    }

    public function update(Request $request, Area $area)
    {
        // Validar los datos del formulario
        $request->validate([
            'nombre' => 'required|string|max:255',
            'descripcion' => 'nullable|string',
        ]);

        $area->update($request->all()); // Actualiza el área
        return redirect()->route('areas.index')
                         ->with('success', 'Área actualizada con éxito.');
    }

    public function destroy(Area $area)
    {
        $area->estado = false; // Cambia el estado a inactivo (eliminación lógica)
        $area->save();

        return redirect()->route('areas.index')
                         ->with('success', 'Área eliminada (cambiada a inactiva).');
    }
}