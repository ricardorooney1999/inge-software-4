<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UsuarioController extends Controller
    {
    // Listar usuarios
        public function listarUsuarios()
            {
                $usuarios = Usuario::select('id_usuario', 'nombre', 'correo_electronico', 'correo_electronico_verificado', 'id_rol','id_area', 'estado', 'created_at', 'updated_at')->get();
        
                return view('main', compact('usuarios'))->with([
                    'content' => 'Gestión de Usuarios',
                    'view' => 'layouts.usuarios.listarUsuarios'
                ]);
            }

        // Generar una contraseña aleatoria
            

        public function createUsuario()
            {
        // Generar una contraseña aleatoria
        $randomPassword = Str::random(10);
        $hashePassword = Hash::make($randomPassword);

          }
}