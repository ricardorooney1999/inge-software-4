<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Usuario extends Authenticatable
{
    use HasFactory, Notifiable;
    
    protected $table = 'usuarios';
    protected $primaryKey = 'id_usuario';

    protected $fillable = [
        'nombre',
        'correo_electronico',
        'password',
        'id_rol',
        'id_area',
        'estado'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getAuthIdentifierName()
    {
        return $this->primaryKey; // 'id_usuario'
    }
    
    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    public function getEmailAttribute()
    {
        return $this->correo_electronico;
    }

    // Nombre de la función en singular porque un usuario tiene un rol
    public function rol() {  
        return $this->belongsTo(Rol::class, 'id_rol', 'id_rol'); // 'id_rol' es la clave foránea en la tabla 'usuarios'
    }
    // Nombre de la función en singular porque un usuario tiene un área
    public function area() { 
        return $this->belongsTo(Area::class, 'id_area', 'id_area'); // 'id_area' es la clave foránea en la tabla 'usuarios'
    }


    // esto es solo para agregar usuario admin la primera vez accede por la ruta /create-admin (no protegido)
    public function createAdminUser()
    {
        $adminEmail = 'admin@admin.com';

        // 1. Verificar si el usuario admin ya existe
        $adminUser = Usuario::where('correo_electronico', $adminEmail)->first();

        if (!$adminUser) {
            // 2. Crear el usuario admin si no existe
            $adminUser = new Usuario;
            $adminUser->nombre = 'Administrador'; // 
            $adminUser->correo_electronico = $adminEmail;
            $adminUser->correo_electronico_verificado = true; // Correo verificado
            $adminUser->password = Hash::make('admin');// Contraseña encriptada
            $adminUser->id_rol = 1; // administrador
            $adminUser->id_area = 1; // área prueba
            $adminUser->estado = true; // estado activo
            
         
            $adminUser->save();

            return redirect('/')->with('success', 'Usuario administrador creado.'); // Redirige o retorna una respuesta
        } else {
            return redirect('/')->with('info', 'El usuario administrador ya existe.');
        }
    }

    public static function obtenerAreasActivas()
    {
        return self::where('estado', true)->distinct()->get(['id_area', 'nombre']);
    }
}