<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Proveedor extends Model
{
    use HasFactory;

    protected $table = 'proveedores'; // Si la tabla no sigue la convención pluralizada
    public $timestamps = true; // Explicitamente habilitado (opcional)


    protected $fillable = [
        'id_proveedores',
        'id_categoria',
        'nombre',
        'ruc',
        'contacto',
        'email',
        'telefono',
        'codigosap',
        'estado'
    ];

    // Método para obtener categorías activas y únicas
    public static function obtenerCategoriasActivas()
    {
        return self::where('estado', true)->distinct()->get(['id_categoria', 'nombre']);
    }
}
