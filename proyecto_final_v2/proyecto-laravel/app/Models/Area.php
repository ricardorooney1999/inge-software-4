<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use HasFactory;
    protected $table = 'areas'; //
    protected $primaryKey = 'id_area'; // Obligatorio si la clave primaria no es 'id'
    protected $fillable = ['nombre', 'descripcion', 'estado']; // Lista los atributos que se pueden asignar masivamente

    // public $timestamps = false; // Solo si no quieres usar timestamps

}