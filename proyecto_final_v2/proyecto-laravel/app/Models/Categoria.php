<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Categoria extends Model
{
    use HasFactory;
    protected $table = 'categorias';
    protected $primaryKey = 'id_categoria'; // Clave primaria personalizada
    public $incrementing = true; // Es autoincremental
    protected $keyType = 'int'; // Es de tipo entero
    protected $fillable = [
        'id_categoria',
        'nombre',
        'estado'
    ];
    public $timestamps = true; //  manejar `created_at` y `updated_at`
    // Relación con el modelo Proveedor
    public function proveedores()
    {
        return $this->hasMany(proveedor::class, 'id_categoria');
    }
    // Método para obtener categorías activas y únicas
    public static function obtenerCategoriasActivas()
    {
        return self::select('estado')->distinct()->get()->pluck('estado');
    }   
}