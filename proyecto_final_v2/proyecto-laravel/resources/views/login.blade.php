<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Aplicación</title>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Agregar ícono de ojo de Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    <style>
        /* Estilo adicional para centrar el formulario */
        .wrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            background-color: #f8f9fa;
        }

        .form-signin {
            width: 100%;
            max-width: 400px;
            padding: 2rem;
            border-radius: 8px;
            background-color: white;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
        }

        .form-signin .form-group {
            position: relative;
        }

        .form-signin i {
            position: absolute;
            top: 50%;
            right: 10px;
            transform: translateY(-50%);
            font-size: 1.2rem;
        }

        .password-input-group {
            position: relative;
        }

        .password-toggle {
            cursor: pointer;
            position: absolute;
            top: 50%;
            right: 10px;
            transform: translateY(-50%);
            font-size: 1.5rem;
        }

        .alert-position {
            position: absolute;
            top: 20px;
            right: 20px;
            z-index: 1050;
        }
    </style>
</head>
<body>
    @if(session('auth_error'))
    <div class="alert alert-danger alert-dismissible fade show alert-position">
        {{ session('auth_error') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <div class="wrapper">
        <form class="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <label for="inputEmail">Correo Electrónico</label>
                <input type="email" 
                    name="correo_electronico" 
                    id="inputEmail" 
                    class="form-control" 
                    value="{{ old('correo_electronico') }}" 
                    required 
                    autofocus>
                    <i class="bi bi-envelope"></i>
            </div>

            <div class="form-group">
                <label for="inputPassword">Contraseña</label>
                <div class="password-input-group">
                    <input type="password" 
                        name="password" 
                        id="inputPassword" 
                        class="form-control" 
                        required>
                    <i class="bi bi-eye-slash password-toggle" 
                    id="togglePassword" 
                    title="Mostrar contraseña"></i>
                </div>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
        </form>
    </div>  
    <script>
        // Auto-ocultar alerta después de 5 segundos
        document.addEventListener('DOMContentLoaded', function() {
            const alert = document.querySelector('.alert');
            if (alert) {
                setTimeout(() => {
                    const bootstrapAlert = bootstrap.Alert.getOrCreateInstance(alert);
                    bootstrapAlert.close();
                }, 5000);
            }

            // Funcionalidad del toggle de contraseña
            document.getElementById('togglePassword').addEventListener('click', function() {
                const passwordInput = document.getElementById('inputPassword');
                const type = passwordInput.getAttribute('type') === 'password' ? 'text' : 'password';
                passwordInput.setAttribute('type', type);
                this.classList.toggle('bi-eye');
                this.classList.toggle('bi-eye-slash');
            });
        });
    </script>
</body>
</html>