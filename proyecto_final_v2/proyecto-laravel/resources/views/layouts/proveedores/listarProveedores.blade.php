    <table id="listarProveedoresTable" data-table="true" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th class="px-4 py-2">Estado</th> 
                <th class="px-4 py-2">ID</th>
                <th class="px-4 py-2">Categoría</th>
                <th class="px-4 py-2">Nombre</th>
                <th class="px-4 py-2">RUC</th>
                <th class="px-4 py-2">Contacto</th>
                <th class="px-4 py-2">Email</th>
                <th class="px-4 py-2">Teléfono</th>
                <th class="px-4 py-2">Código SAP</th>
                <th class="px-4 py-2">Acción</th>
                <th class="px-4 py-2">Creado</th>
                <th class="px-4 py-2">Actualizado</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($proveedores as $proveedor)
                <tr> 
                <td class="border px-4 py-2">
                        @if($proveedor->estado)
                            <span class="badge bg-success">Activo</span>
                        @else
                            <span class="badge bg-danger">Inactivo</span>
                        @endif
                    </td>                   
                    <td class="border px-4 py-2">{{ $proveedor->id_proveedor }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->id_categoria }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->nombre }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->ruc }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->contacto }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->email }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->telefono }}</td>
                    <td class="border px-4 py-2">{{ $proveedor->codigosap }}</td>
                    <td class="text-center">
                        <div class="d-flex justify-content-center align-items-center gap-2">
                            <button class="btn btn-info">
                                <i class="bi bi-pencil-square"></i>
                            </button>
                            <button class="btn btn-danger">
                                <i class="bi bi-trash"></i>
                            </button>
                        </div>
                    </td>
                    <td>{{ $proveedor->created_at }}</td>
                    <td>{{ $proveedor->updated_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>