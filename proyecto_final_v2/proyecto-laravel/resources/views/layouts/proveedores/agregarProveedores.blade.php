<!-- Formulario para agregar proveedores -->
<form>
    <div class="mb-3">
        <!-- Contenedor flexible para mover el switch a la derecha -->
        <div class="d-flex justify-content-between align-items-center">
            <label for="estado">Estado:</label>
            <div class="form-check form-switch">
                <input type="checkbox" class="form-check-input" id="estado" name="estado" 
                    value="1" 
                    {{ old('estado', $categoria->estado ?? null) ? 'checked' : '' }}>
                <label class="form-check-label" for="estado"></label>
            </div>
        </div>

        <label for="nombre" class="form-label">Nombre del proveedor</label>
        <input type="text" class="form-control" id="nombre" name="nombre">

        <label for="disabledSelect" class="form-label">Seleccione una categoría</label>
        <select id="disabledSelect" class="form-select" name="categoria_id">
            @foreach($categorias as $categoria)
                <option value="{{ $categoria->id_categoria }}">{{ $categoria->nombre }}</option>
            @endforeach
        </select>

        <label for="ruc" class="form-label">RUC</label>
        <input type="text" class="form-control" id="ruc" name="ruc">

        <label for="sap" class="form-label">Código SAP</label>
        <input type="text" class="form-control" id="sap" name="sap" oninput="this.value = this.value.toUpperCase();">
        
        <label for="contacto" class="form-label">Nombre del contacto</label>
        <input type="text" class="form-control" id="contacto" name="contacto">

        <label for="emailProveedor" class="form-label">Correo</label>
        <input type="email" class="form-control" id="emailProveedor" placeholder="correo@delProveedor.com" name="emailProveedor">
    </div>

    <!-- Botón de envío -->
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>

<!-- ESTILO DE ESTADO PARA EL SWITCH -->
<style>
    .form-check-input:checked {
        width: 3.5rem !important;    /* Ancho total */
        height: 1.5rem !important;   /* Altura total */
        background-color: #198754 !important;
        border-color: #198754 !important;
    }
    .form-check-input:not(:checked) {
        width: 3.5rem !important;    /* Ancho total */
        height: 1.5rem !important;   /* Altura total */
        background-color: #dc3545 !important;
        border-color: #dc3545 !important;
    }
</style>
