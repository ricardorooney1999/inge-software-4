<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HOME</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- DataTables CSS -->
    <link rel="stylesheet" href="https://cdn.datatables.net/2.2.1/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.1/css/buttons.bootstrap5.min.css">
    <!-- Estilo del navbar -->
    <link rel="stylesheet" href="{{ asset('css/navbar.css') }}">
    <!-- Iconos de bootstrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="hero">
    <nav class="navbar navbar-expand-lg shadow-sm">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <svg xmlns="http://www.w3.org/2000/svg" height="48px" viewBox="0 -960 960 960" width="48px" fill="#000000">
                    <path d="m480-574-42-42 74-74H330v-60h182l-74-74 42-42 146 146-146 146ZM289.79-80Q260-80 239-101.21t-21-51Q218-182 239.21-203t51-21Q320-224 341-202.79t21 51Q362-122 340.79-101t-51 21Zm404 0Q664-80 643-101.21t-21-51Q622-182 643.21-203t51-21Q724-224 745-202.79t21 51Q766-122 744.79-101t-51 21ZM62-820v-60h116l170 364h287.71L796-796h67L701-493q-11 19-28.56 30.5T634-451H331l-56 104h491v60H284q-37.66 0-57.33-30T224-378l64-118-148-324H62Z"/>
                </svg>
            </a>
            <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
                <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                           aria-expanded="false">
                            Administrador
                        </a>
                        <div class="dropdown-menu px-3 rounded-3 border-0 shadow"
                             style="font-size: 0.9rem; max-width: 300px;">
                            <div class="row">
                                <div class="col-12">
                                    <a href="{{ route('listarUsuarios') }}">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-warning-subtle rounded-3 fs-1">
                                                <i class="bi bi-tv"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>Usuarios</h5>
                                                <div>CRUD de usuarios.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('listarProveedores') }}">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-success-subtle rounded-3 fs-1">
                                                <i class="bi bi-truck"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>Proveedores</h5>
                                                <div>CRUD de Proveedores.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-12">
                                    <a href="{{ route('categorias.all') }}"
                                       class="d-flex align-items-center py-3 px-1 rounded-3">
                                        <div class="icon px-3 bg-body-tertiary rounded-3 fs-1">
                                            <i class="bi bi-inboxes-fill"></i>
                                        </div>
                                        <div class="text ps-3">
                                            <h5>Categorias</h5>
                                            <div>CRUD de Categorias.</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav ms-auto">
                    @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                               data-bs-toggle="dropdown" aria-expanded="false">
                                Bienvenido, {{ Auth::user()->nombre }}
                            </a>
                            <div class="dropdown-menu px-3 rounded-3 border-0 shadow" style="font-size: 0.9rem;max-width: 200px;">
                                <div class="row">
                                    <div class="col-12">
                                        <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                                <div class="icon px-3 bg-primary-subtle rounded-3 fs-1">
                                                    <i class="bi bi-x-octagon-fill"></i>
                                                </div>
                                                <div class="text ps-3">
                                                    <h5>Salir</h5>
                                                    <div>Cerrar la aplicacion.</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endauth
                </ul>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </div>
    </nav>
    @if (!empty($content))
        <div class="container mt-4">
            <div class="card">
                <div class="card">
                    <div class="card-header">
                        <h2>{{ $content }}</h2>
                    </div>
                    <div class="card-body">
                        @yield('content')
                    </div>
                </div>
        </div>
    @endif
</div>

<div class="modal-dialog modal-dialog-centered">
  ...
</div>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <!-- DataTables y Bootstrap -->
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <!-- pdfMake (necesario para PDF) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
    <!-- Botones de DataTables -->
    <script src="https://cdn.datatables.net/buttons/2.4.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.4.1/js/buttons.print.min.js"></script>
    <!-- JSZip (necesario para Excel) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
    <!-- @stack('scripts') -->
</body>
</html>
