<form action="{{ route('usuarios.store') }}" method="POST">
    @csrf
<!-- Formulario para agregar usuarios -->
    <div class="d-flex justify-content-between align-items-center">
        <label for="estado">Estado:</label>
        <div class="form-check form-switch">
            <input type="checkbox" class="form-check-input" id="estado" name="estado" 
                value="1" 
                {{ old('estado', $usuario->estado ?? null) ? 'checked' : '' }}>
            <label class="form-check-label" for="estado"></label>  
        </div>       
    </div>

    <div class="mb-3">
        <label for="name" class="form-label">Nombre del usuario</label>
        <input type="text" class="form-control" id="nombre" name="nombre">
    </div>

    <div class="mb-3">
        <label for="last_name" class="form-label">Apellido del usuario</label>
        <input type="text" class="form-control" id="apellido" name="apellido">
    </div>

    <label for="disabledSelect" class="form-label">Area</label>
        <select id="disabledSelect" class="form-select" name="areas"> 

        </select>

    <div class="mb-3">
        <label for="area" class="form-label">Rol</label>
        <input type="text" class="form-control" id="area" name="area">
    </div>

    <div class="mb-3">
        <label for="email" class="form-label">Correo</label>
        <input type="email" class="form-control" id="email" placeholder="correo@deLaEmpresa.com" name="email">
    </div>

    <!-- Botón de envío -->
    <button type="submit" class="btn btn-primary">Agregar</button>
</form>

<!-- ESTILO DE ESTADO PARA EL SWITCH -->
<style>
    .form-check-input:checked {
        width: 3.5rem !important;    /* Ancho total */
        height: 2rem !important;     /* Altura total */
        background-color: #198754 !important;
        border-color: #198754 !important;
    }
    .form-check-input:not(:checked) {
        width: 3.5rem !important;    /* Ancho total */
        height: 2rem !important;     /* Altura total */
        background-color: #dc3545 !important;
        border-color: #dc3545 !important;
    }
</style>