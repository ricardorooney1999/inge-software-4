<table id="listarUsuariosTable" data-table="true" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Estado</th>
            <th>ID</th>
            <th>Nombre</th>
            <th>Email</th>
            <th>Email Verificado</th>
            <th>Rol</th>
            <th>Area</th>
            <th class="no-filter">Acción</th>
            <th>Creado</th>
            <th>Actualizado</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($usuarios as $usuario)
            <tr>
            <td class="text-center">
                    @if ($usuario->delete_at)
                        <span class="badge bg-danger">Inactivo</span>
                    @else
                        <span class="badge bg-success">Activo</span>
                    @endif
                </td>
                <td>{{ $usuario->id_usuario }}</td>
                <td>{{ $usuario->nombre }}</td>
                <td>{{ $usuario->correo_electronico }}</td>
                <td class="text-center">
                    @if ($usuario->correo_electronico_verificado)
                        <span class="badge bg-success">Sí</span>
                    @else
                        <span class="badge bg-danger">No</span>
                    @endif
                </td>

                <td>{{ $usuario->rol->nombre }}</td>
                <td>{{ $usuario->area->nombre }}</td>
                <td class="text-center">
                        <div class="d-flex justify-content-center align-items-center gap-2">
                            <button class="btn btn-info">
                                <i class="bi bi-pencil-square"></i>
                            </button>
                            <button class="btn btn-danger">
                                <i class="bi bi-trash"></i>
                            </button>
                        </div>
                    </td>
                    <td>{{ $usuario->created_at }}</td>
                    <td>{{ $usuario->updated_at }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
