@extends('layouts.app')
   
   <table id="listarCategoriasTable" data-table="true" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Estado</th>
                <th>ID</th>
                <th>Nombre</th>
                <th class="no-filter">Acción</th>
                <th>Creado</th>
                <th>Actualizado</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categorias as $categoria)
                <tr>
                    <td class="text-center">
                        @if ($categoria->estado)
                            <span class="badge bg-success">Activo</span>
                        @else
                            <span class="badge bg-danger">Inactivo</span>
                        @endif
                    </td>
                    <td>{{ $categoria->id_categoria }}</td>
                    <td>{{ $categoria->nombre }}</td>
                    <td class="text-center">
                        <div class="d-flex justify-content-center align-items-center gap-2">
                            <button class="btn btn-info">
                                <i class="bi bi-pencil-square"></i>
                            </button>
                            <button class="btn btn-danger">
                                <i class="bi bi-trash"></i>
                            </button>
                        </div>
                    </td>
                    <td>{{ $categoria->created_at }}</td>
                    <td>{{ $categoria->updated_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>