@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-12">
            <a href="{{ route('categorias.all') }}" class="btn btn-primary btn-lg mb-4">
                <i class="bi bi-arrow-left-square"></i> Volver
            </a>

            <div class="card">
                <!-- ALERTAS -->
                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show auto-close" role="alert">
                            {{ session('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    
                    @if(session('error'))
                        <div class="alert alert-danger alert-dismissible fade show auto-close" role="alert">
                            {{ session('error') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    @if($errors->has('nombre'))
                        <div class="alert alert-danger alert-dismissible fade show auto-close" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            {{ $errors->first('nombre') }}
                        </div>
                    @endif
                    <!-- FORMULARIO -->
                    <form action="{{ route('categorias.store') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre de la categoría</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" required>
                        </div>
                        <div class="mb-3">
                            <label for="estado" class="form-label">Estado:</label>
                            <input type="checkbox" class="form-check-input" id="estado" name="estado" value="1" checked>
                        </div>
                        <button type="submit" class="btn btn-success btn-lg mb-4">
                            <i class="bi bi-floppy2"></i> Guardar
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function() {
        let alerts = document.querySelectorAll('.auto-close'); 

        alerts.forEach(alert => {
            setTimeout(() => {
                new bootstrap.Alert(alert).close(); 
            }, 3000); // 3000 milliseconds = 3 seconds, NO FUNCIONA
        });
    });
</script>