<div class="dropdown-menu px-3 rounded-3 border-0 shadow">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{ route('listarUsuarios') }}">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-warning-subtle rounded-3 fs-1">
                                                <i class="bi bi-tv"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>Usuarios</h5>
                                                <div>Irure incididunt eu irure quis ipsum
                                                    occaecat dolor quis.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                                <div class="col-sm-6">
                                    <a href="#">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-danger-subtle rounded-3 fs-1">
                                                <i class="bi bi-headphones"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>xxxx</h5>
                                                <div>yyy</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{ route('listarProveedores') }}">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-success-subtle rounded-3 fs-1">
                                                <i class="bi bi-truck"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>Proveedores</h5>
                                                <div>Irure incididunt eu irure quis ipsum
                                                    occaecat dolor quis.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="#">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-secondary-subtle rounded-3 fs-1">
                                                <i class="bi bi-laptop"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>xxx</h5>
                                                <div>yyyy</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="{{route( name:'categorias.all')}}" class="d-flex align-items-center py-3 px-1 rounded-3">
                                        <div class="icon px-3 bg-body-tertiary rounded-3 fs-1">
                                            <i class="bi bi-inboxes-fill"></i>
                                        </div>
                                        <div class="text ps-3">
                                            <h5>Categorias</h5>
                                            <div>Irure incididunt eu irure quis ipsum occaecat dolor quis.</div>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-sm-6">
                                    <a href="#">
                                        <div class="d-flex align-items-center py-3 px-1 rounded-3">
                                            <div class="icon px-3 bg-info-subtle rounded-3 fs-1">
                                                <i class="bi bi-earbuds"></i>
                                            </div>
                                            <div class="text ps-3">
                                                <h5>xxx</h5>
                                                <div>yyy</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>