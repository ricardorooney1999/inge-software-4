@extends('layouts.app')

@section('content')
<div class="container mt-4"> <!-- Contenedor PRINCIPAL -->
    <!-- ALERTAS -->
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif

    <div class="row"> <!-- Fila Bootstrap -->
        <div class="col-12"> <!-- Columna para contenido -->
            @isset($view)
                @include($view)
            @endisset
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <!-- JS de DataTables.net -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> -->
    <script>
        $(document).ready(function() {
            $('table[data-table="true"]').each(function () {
                const tableId = $(this).attr('id');
                console.log('ID de la tabla:', tableId);
                if ($.fn.DataTable.isDataTable('#' + tableId)) {
                    $('#' + tableId).DataTable().destroy();
                }
                $('#' + tableId).DataTable({
                    "language": {
                        "lengthMenu": "Mostrar _MENU_ registros por página", // Texto para el menú
                        "zeroRecords": "No se encontraron registros",
                        "info": "Mostrando la página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de _MAX_ registros totales)",
                        "search": "Buscar:",
                        "paginate": {
                            "first": "Primero",
                            "last": "Último",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        },
                        "buttons": {
                            "copy": "Copiar",
                            "print": "Imprimir"
                        }
                    },
                    dom: '<"d-flex justify-content-between align-items-center"<"text-start"l><"text-center"B><"text-end"f>>rtip',
                    buttons: [
                        {
                        text: 'Agregar',
                        className: 'btn btn-success',
                             action: function () {
                                let url = '';
                                if (tableId === 'listarProveedoresTable') {
                                    url = '{{ route("proveedores.create") }}';
                                } else if (tableId === 'listarUsuariosTable') {
                                    //
                                } else if (tableId === 'listarCategoriasTable') {
                                    url = '{{ route("categorias.create") }}';
                                }

                                if (url) {
                                    window.location.href = url;
                                } else {
                                    console.log("No se encontró la URL para la tabla: " + tableId);
                                }
                            }
                        },
                        'excel', 'pdf', 'print'
                    ]
                });
            });
        });
    </script>
@endpush

@push('styles')
<!-- archivos CSS de DataTables.net -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <!-- .dataTables_wrapper .dt-buttons {
    margin-bottom: 0 !important;
    }

    .dataTables_wrapper .dataTables_filter {
        margin-top: 0 !important;
    } -->
@endpush