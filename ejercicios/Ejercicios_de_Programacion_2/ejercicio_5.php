<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios de Programación 2 – Ej5</title>
    <link rel="stylesheet" href="estilos.css">

</head>
<body>
<div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
    <p>Hacer un script PHP el cual utilice el operador ternario de PHP para realizar lo siguiente:</p>
        <ul>
            <li>Se deben declarar tres variables y le asignan valores enteros aleatorios (los valores deben
            estar entre 99 y 999). Las variables serán $a, $b y $c</li>
            <li>Si la expresión $a*3 > $b+$c se debe imprimir que la expresión $a*3 es mayor que la
            expresión $b+$c</li>
            <li>Si la expresión $a*3 <= $b+$c se debe imprimir que la expresión $b+$c es mayor o igual
            que la expresión $a*3</li>
        </ul>
        <?php
        $a = rand(99, 999);
        $b = rand(99, 999);
        $c = rand(99, 999);
        $res = ($a * 3 > $b + $c) ? "$a*3 es mayor que $b+$c" : "$b+$c es mayor o igual que $a*3";
        ?>
        <p>Resultado: <?php echo $res; ?></p>
    </div>
</body>
</html>
