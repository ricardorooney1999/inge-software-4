<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios de Programación #2 – Ej4</title>
    <link rel="stylesheet" href="estilos.css">
    <style>
        .gray {
            background-color: #cccccc;
        }
        .white {
            background-color: #ffffff;
        }
    </style>    
</head>
<body>
    <div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
        <p>Hacer un script en PHP que haga lo siguiente:</p>
        <ul>
            <li>El script PHP debe estar embebido en una página HTML</li>
            <li>Hacer un script en PHP que muestre en pantalla la tabla de multiplicar el 9, coloreando las
            filas alternando gris y blanco</li>
        </ul>
        <!-- Insertar el código PHP aquí -->
        <?php
            echo "<table border='1'>";
            for ($i = 1; $i <= 10; $i++) {
                // Alternar el color de fondo de las filas
                $color = ($i % 2 == 0) ? 'gray' : 'white';
                echo "<tr class='$color'>";
                echo "<td>9 x $i</td>";
                echo "<td>" . 9 * $i . "</td>";
                echo "</tr>";
            }
            echo "</table>";
        ?>
    </div>
</body>
</html>