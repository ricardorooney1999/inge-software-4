<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios de Programación #2 – Ej8</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
        <p>Hacer un script en PHP que genere números aleatorios hasta que el número generado sea divisible
por 983. Cuando ocurra esta condición imprimir un mensaje. <br>
Se debe usar un ciclo infinito</p>
<?php
        while (true) {
            $numAleatorio = rand(1, 100000);
            if ($numAleatorio % 983 === 0) {
                echo "<p>El número $numAleatorio es divisible por 983.</p>";
                break;
            }
        }
        ?>

    </div>
</body>
</html>