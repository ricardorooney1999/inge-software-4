<!DOCTYPE html>
<html>
<head>
    <title> Ejercicios de Programación #2 – Ej3</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
    <p>Hacer un script en PHP que haga lo siguiente:</p>  
        <ul>
            <li>El script PHP debe estar embebido en una página HTML</li>
            <li>Crear una variable con el siguiente contenido 24.5</li>
            <li>Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla
            (poner un carácter de nueva línea)</li>
            <li>En la siguiente línea del script, modificar el contenido de la variable al valor “HOLA”</li>
            <li>Determinar cuál es el tipo de datos que posee esta variable e imprimirlo en pantalla
            (poner un carácter de nueva línea)</li>
            <li>Setear el tipo de la variable que contiene “HOLA” a un tipo entero</li>
            <li>Determinar con var_dump el contenido y tipo de esa variable}</li>
        </ul>
        <?php
        $var = 24.5;
        echo $var . ' Tipo de datos de la variable: ' . gettype($var) . "<br>";
        $var = "HOLA";
        echo $var . ' Tipo de datos de la variable después de la modificación: ' . gettype($var) . "<br>";
        settype($var, "integer");
        echo $var . ' Contenido y tipo de la variable después de setearla como entero: ';
        var_dump($var);
        ?>
    </div>
</body>
</html>
