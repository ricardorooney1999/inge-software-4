<!DOCTYPE html>
<html>
<head>
    <title> Ejercicios de Programación #2 – Ej1</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
    <p>Hacer un script PHP que haga lo siguiente:</p>
        <ul>
            <li>El script PHP debe estar embebido en una página HTML.</li>
            <li>Crear una variable que almacene su nombre y apellido.</li>
            <li>Crear una variable que guarde su nacionalidad.</li>
            <li>Usar la función echo para imprimir en pantalla el contenido de la variable que almacena su
            nombre y apellido. <br>El contenido se debe desplegar en negrita y en color rojo.</li>
            <li>Usar la función print para imprimir en pantalla el contenido de la variable que almacena su nacionalidad.<br>
            El contenido se debe desplegar subrayado.
            </li>
        </ul>
        <?php
        $nom = "Ricardo Benítez";
        $nac = "Paraguaya";
        echo '<p><span style="font-weight: bold; color: red;">' . $nom . '</span></p>';
        print '<p><u>' . $nac . '</u></p>';
        ?>
    </div>
</body>
</html>