<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios de Programación #2 – Ej2</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
        <p>Hacer un script PHP que haga lo siguiente:</p>       
        <ul>
            <li>El script PHP debe estar embebido en una página HTML</li>
            <li>Hacer un script PHP que concatene dos cadenas con el operador punto (.) e imprimir su
            resultado.<br> Cada cadena debe estar contenida en una variable diferente.</li>
        </ul>
        <?php
        $cad1 = "Amo, ";
        $cad2 = "IS4!";
        $res = $cad1 . $cad2;
        echo '<p>' . $res . '</p>';
        ?>
    </div>
</body>
</html>
