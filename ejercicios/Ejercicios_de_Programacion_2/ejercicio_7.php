<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios de Programación 2 – Ej7</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>
    <div class="contenido">
        <p>Hacer un script en PHP que imprima 900 números aleatorios pares. <br>
Se deben generar números aleatorios entre 1 y 10.000</p>
<?php
$numeros = array();
while (count($numeros) < 900) {
    
    $numero = rand(1, 10000);
    
  
    if ($numero % 2 == 0) {
       
        $numeros[] = $numero;
    }
}
foreach ($numeros as $numero) {
    echo $numero . " / ";
}
?>
    </div>
</body>
</html>