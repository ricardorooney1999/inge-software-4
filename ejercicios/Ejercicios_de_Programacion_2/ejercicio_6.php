
<!DOCTYPE html>
<html>
<head>
    <title>Ejercicios de Programación 2 – Ej6</title>
    <link rel="stylesheet" href="estilos.css">

</head>
<body>
<div class="centrado-izquierda">
        <h2><a href="index.html">EJERCITARIO 2</a></h2>
    </div>

    <div class="contenido">
    <p>Hacer un script PHP, utilizando la estructura de selección swicth que realice lo siguiente:</p>        
        <ul>
            <li>Se deben definir tres variables correspondientes a las notas de un alumno en un curso de
            PHP</li>
            <li>La variable parcial1 puede tener un valor entre 0 y 30 (Se debe generar un valor aleatorio)</li>
            <li>La variable parcial2 puede tener un valor entre 0 y 20 (Se debe generar un valor aleatorio)</li>
            <li>La variable final1 puede tener un valor entre 0 y 50 (Se debe generar un valor aleatorio)</li>
        </ul>
        <p>Se deben sumar los tres acumulados (en la expresión del switch) e imprimir si el alumno tuvo nota
1 o 2 o 3 o 4 o 5 <br>
Nota 1: entre 0 y 59<br>
Nota 2: entre 60 y 69<br>
Nota 3: entre 70 y 79<br>
Nota 4: entre 80 y 89<br>
Nota 5: entre 90 y 100</p>
<?php
        $parcial1 = rand(0, 30);
        $parcial2 = rand(0, 20);
        $final1 = rand(0, 50);
        $notaTotal = $parcial1 + $parcial2 + $final1;
        $calificacion = "";
        switch (true) {
            case ($notaTotal >= 90 && $notaTotal <= 100):
                $calificacion = "5";
                break;
            case ($notaTotal >= 80 && $notaTotal <= 89):
                $calificacion = "4";
                break;
            case ($notaTotal >= 70 && $notaTotal <= 79):
                $calificacion = "3";
                break;
            case ($notaTotal >= 60 && $notaTotal <= 69):
                $calificacion = "2";
                break;
            default:
                $calificacion = "1";
        }
        echo "<p>Su puntaje es $notaTotal</p> <p>Calificación: $calificacion</p>";
        ?>
        
    </div>
</body>
</html>
