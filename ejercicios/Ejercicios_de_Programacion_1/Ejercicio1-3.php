<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Productos</title>
    <link rel="stylesheet" href="/inge-software-4/estilos.css">
    <style>
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: center;
        }
        .fondo {
            background-color: #f2f2f2;
        }
        .titulo {
            font-size: 24px;
            font-weight: bold;
            background-color: yellow;
            color: black;
            text-align: center;
        }
        .F1 {
            background-color: white; 
        }
        .F2 {
            background-color: lightgreen; 
        }
    </style>
</head>
<body>
    <div class="centrado-izquierda">
        <a href="/inge-software-4/index.html"><H1>INGENIERIA DE SOFTWARE 4</H1></a>
        <h2><a href="/inge-software-4/Ejercicios_de_Programacion_1/ejercicio1-3.php">EJERCITARIO 1</a></h2>
        <h2><a href="/inge-software-4/Ejercicios_de_Programacion_2">EJERCITARIO 2</a></h2>
    </div>
    <div class="centrado-arriba">
        <img src="/inge-software-4/baner_proyecto.jfif" alt="JAHA HESE">
    </div>
    <div class="contenido">
        <?php
            echo '<table>';
            echo '<tr><th colspan="3" class="titulo">Productos</th></tr>';
            echo '<tr>';
            echo '<th class="fondo">Nombre</th>';
            echo '<th class="fondo">Cantidad</th>';
            echo '<th class="fondo">Precio (Gs.)</th>';
            echo '</tr>';
            // filas de datos
            echo '<tr class ="F1">';
            echo '<td>Coca Cola</td>';
            echo '<td>100</td>';
            echo '<td>4.500</td>';
            echo '</tr>';

            echo '<tr class ="F2">';
            echo '<td>Pepsi</td>';
            echo '<td>30</td>';
            echo '<td>4.800</td>';
            echo '</tr>';

            echo '<tr class ="F1">';
            echo '<td>Sprite</td>';
            echo '<td>20</td>';
            echo '<td>4.500</td>';
            echo '</tr>';

            echo '<tr class ="F2">';
            echo '<td>Guarana</td>';
            echo '<td>200</td>';
            echo '<td>4.500</td>';
            echo '</tr>';

            echo '<tr class ="F1">';
            echo '<td>SevenUP</td>';
            echo '<td>24</td>';
            echo '<td>4.800</td>';
            echo '</tr>';

            echo '<tr class ="F2">';
            echo '<td>Mirinda Naranja</td>';
            echo '<td>56</td>';
            echo '<td>4.800</td>';
            echo '</tr>';

            echo '<tr class ="F1">';
            echo '<td>Mirinda Guaraná</td>';
            echo '<td>89</td>';
            echo '<td>4.800</td>';
            echo '</tr>';

            echo '<tr class ="F2">';
            echo '<td>Fanta Naranja</td>';
            echo '<td>10</td>';
            echo '<td>4.500</td>';
            echo '</tr>';

            echo '<tr class ="F1">';
            echo '<td>Fanta Piña</td>';
            echo '<td>2</td>';
            echo '<td>4.500</td>';
            echo '</tr>';

            echo '</table>';
        ?>
    </div>
</body>
</html>

