<?php
// Cargar Composer autoload
require 'vendor/autoload.php';

// Configuración de la conexión a la base de datos
$host = 'localhost';
$dbname = 'bdEjercitario4';
$user = 'postgres';
$password = 'admin';

try {
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Sesion iniciada corectamente.\n";
} catch (PDOException $e) {
    die("No se puede conectar a la base de datos: " . $e->getMessage());
}

// Parámetros de paginación
$registros_por_pagina = 20;
$pagina_actual = isset($_GET['pagina']) ? intval($_GET['pagina']) : 1;
$offset = ($pagina_actual - 1) * $registros_por_pagina;

// Filtros
$filtros = [];
$condiciones = [];

// Filtro por código de institución
if (!empty($_GET['codigo_institucion'])) {
    $codigo_institucion = $_GET['codigo_institucion'];
    $condiciones[] = "i.codigo_institucion LIKE :codigo_institucion";
    $filtros['codigo_institucion'] = '%' . $codigo_institucion . '%';
}

// Filtro por nombre de institución
if (!empty($_GET['nombre_institucion'])) {
    $nombre_institucion = $_GET['nombre_institucion'];
    $condiciones[] = "i.nombre_institucion LIKE :nombre_institucion";
    $filtros['nombre_institucion'] = '%' . $nombre_institucion . '%';
}

// Filtro por departamento
if (!empty($_GET['departamento'])) {
    $departamento = $_GET['departamento'];
    $condiciones[] = "d.nombre_departamento LIKE :departamento";
    $filtros['departamento'] = '%' . $departamento . '%';
}

// Filtro por distrito
if (!empty($_GET['distrito'])) {
    $distrito = $_GET['distrito'];
    $condiciones[] = "di.nombre_distrito LIKE :distrito";
    $filtros['distrito'] = '%' . $distrito . '%';
}

// Filtro por localidad/barrio
if (!empty($_GET['localidad'])) {
    $localidad = $_GET['localidad'];
    $condiciones[] = "l.nombre_barrio_localidad LIKE :localidad";
    $filtros['localidad'] = '%' . $localidad . '%';
}

// Filtro por zona
if (!empty($_GET['zona'])) {
    $zona = $_GET['zona'];
    $condiciones[] = "z.nombre_zona LIKE :zona";
    $filtros['zona'] = '%' . $zona . '%';
}

// Generar la consulta SQL con filtros y paginación
$sql = "SELECT i.codigo_institucion, i.nombre_institucion, d.nombre_departamento, di.nombre_distrito, 
        l.nombre_barrio_localidad, z.nombre_zona
        FROM Institucion i
        JOIN Directorio dir ON i.id_institucion = dir.id_institucion
        JOIN Zona z ON dir.id_zona = z.id_zona
        JOIN Localidad l ON z.id_localidad = l.id_localidad
        JOIN Distrito di ON l.id_distrito = di.id_distrito
        JOIN Departamento d ON di.id_departamento = d.id_departamento";

if (!empty($condiciones)) {
    $sql .= " WHERE " . implode(" AND ", $condiciones);
}

$sql .= " LIMIT :limit OFFSET :offset";

$stmt = $pdo->prepare($sql);

// Asignar los valores de los filtros
foreach ($filtros as $key => $value) {
    $stmt->bindValue(':' . $key, $value);
}
$stmt->bindValue(':limit', $registros_por_pagina, PDO::PARAM_INT);
$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);

$stmt->execute();
$instituciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Obtener el número total de registros para la paginación
$sql_count = "SELECT COUNT(*) FROM Institucion i
              JOIN Directorio dir ON i.id_institucion = dir.id_institucion
              JOIN Zona z ON dir.id_zona = z.id_zona
              JOIN Localidad l ON z.id_localidad = l.id_localidad
              JOIN Distrito di ON l.id_distrito = di.id_distrito
              JOIN Departamento d ON di.id_departamento = d.id_departamento";
if (!empty($condiciones)) {
    $sql_count .= " WHERE " . implode(" AND ", $condiciones);
}
$stmt_count = $pdo->prepare($sql_count);

// Asignar los valores de los filtros
foreach ($filtros as $key => $value) {
    $stmt_count->bindValue(':' . $key, $value);
}
$stmt_count->execute();
$total_registros = $stmt_count->fetchColumn();
$total_paginas = ceil($total_registros / $registros_por_pagina);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Listado de Instituciones</title>
</head>
<body>
    <h1>Listado de Instituciones</h1>

    <!-- Formulario de filtros -->
    <form method="GET" action="">
        <input type="text" name="codigo_institucion" placeholder="Código Institución" value="<?= htmlspecialchars($_GET['codigo_institucion'] ?? '') ?>">
        <input type="text" name="nombre_institucion" placeholder="Nombre Institución" value="<?= htmlspecialchars($_GET['nombre_institucion'] ?? '') ?>">
        <input type="text" name="departamento" placeholder="Departamento" value="<?= htmlspecialchars($_GET['departamento'] ?? '') ?>">
        <input type="text" name="distrito" placeholder="Distrito" value="<?= htmlspecialchars($_GET['distrito'] ?? '') ?>">
        <input type="text" name="localidad" placeholder="Localidad/Barrio" value="<?= htmlspecialchars($_GET['localidad'] ?? '') ?>">
        <input type="text" name="zona" placeholder="Zona" value="<?= htmlspecialchars($_GET['zona'] ?? '') ?>">
        <button type="submit">Filtrar</button>
    </form>

    <!-- Tabla de instituciones -->
    <table border="1">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Departamento</th>
                <th>Distrito</th>
                <th>Localidad/Barrio</th>
                <th>Zona</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($instituciones as $institucion): ?>
                <tr>
                    <td><?= htmlspecialchars($institucion['codigo_institucion']) ?></td>
                    <td><?= htmlspecialchars($institucion['nombre_institucion']) ?></td>
                    <td><?= htmlspecialchars($institucion['nombre_departamento']) ?></td>
                    <td><?= htmlspecialchars($institucion['nombre_distrito']) ?></td>
                    <td><?= htmlspecialchars($institucion['nombre_barrio_localidad']) ?></td>
                    <td><?= htmlspecialchars($institucion['nombre_zona']) ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <!-- Paginación -->
    <div>
        <?php
        // Crear la cadena de consulta con los filtros actuales
        $query_string = http_build_query(array_merge($_GET, ['pagina' => '']));

        // Variables para la paginación segmentada de 10 en 10
        $segmento_paginas = 10;
        $inicio_segmento = floor(($pagina_actual - 1) / $segmento_paginas) * $segmento_paginas + 1;
        $fin_segmento = min($inicio_segmento + $segmento_paginas - 1, $total_paginas);

        // Botón de "anterior" para el bloque de 10 páginas
        if ($inicio_segmento > 1): ?>
            <a href="?<?= $query_string . ($inicio_segmento - 1) ?>">&laquo; << </a>
        <?php endif;

        // Mostrar números de páginas dentro del segmento actual
        for ($i = $inicio_segmento; $i <= $fin_segmento; $i++): ?>
            <a href="?<?= $query_string . $i ?>" <?= $i == $pagina_actual ? 'style="font-weight:bold;"' : '' ?>><?= $i ?></a>
        <?php endfor;

        // Botón de "siguiente" para el bloque de 10 páginas
        if ($fin_segmento < $total_paginas): ?>
            <a href="?<?= $query_string . ($fin_segmento + 1) ?>"> >> &raquo;</a>
        <?php endif; ?>
    </div>
</body>
</html>
