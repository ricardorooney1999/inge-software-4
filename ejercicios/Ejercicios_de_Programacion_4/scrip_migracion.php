<?php
// Conexión a la base de datos PostgreSQL
$host = 'localhost';
$dbname = 'bdEjercitario4';
$user = 'postgres';
$password = 'admin';

try {
    $pdo = new PDO("pgsql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Error al conectar con la base de datos: " . $e->getMessage());
}

// Función para validar enteros, devuelve null si el valor es una cadena vacía
function validarEntero($valor) {
    return (is_numeric($valor) && $valor !== '') ? (int)$valor : null;
}

try {
    // Comienza una transacción para asegurar la consistencia de los datos
    $pdo->beginTransaction();

    // Eliminar datos de la tabla Directorio
    $pdo->exec("DELETE FROM Directorio");

    // Eliminar datos de la tabla Institucion
    $pdo->exec("DELETE FROM Institucion");

    // Eliminar datos de la tabla Establecimiento
    $pdo->exec("DELETE FROM Establecimiento");

    // Eliminar datos de la tabla Zona
    $pdo->exec("DELETE FROM Zona");

    // Eliminar datos de la tabla Localidad
    $pdo->exec("DELETE FROM Localidad");

    // Eliminar datos de la tabla Distrito
    $pdo->exec("DELETE FROM Distrito");

    // Eliminar datos de la tabla Departamento
    $pdo->exec("DELETE FROM Departamento");

    // Eliminar datos de la tabla Periodo
    $pdo->exec("DELETE FROM Periodo");

    // Confirmar la transacción
    $pdo->commit();

} catch (Exception $e) {
    // En caso de error, revertir los cambios
    $pdo->rollBack();
    die("Error al eliminar datos: " . $e->getMessage());
}

// Leer el archivo JSON
$jsonFile = __DIR__ . '/datos.json';
 // Cambia esta ruta a la correcta
$jsonData = file_get_contents($jsonFile);
$instituciones = json_decode($jsonData, true);

// Insertar los datos en las tablas
foreach ($instituciones as $institucion) {
    try {
        // Insertar Periodo
        $queryPeriodo = "SELECT id_periodo FROM Periodo WHERE anho = :anho";
        $stmtPeriodo = $pdo->prepare($queryPeriodo);
        $stmtPeriodo->execute([':anho' => validarEntero($institucion['periodo'])]);
        $id_periodo = $stmtPeriodo->fetchColumn();

        if (!$id_periodo) {
            $queryPeriodoInsert = "INSERT INTO Periodo (anho) VALUES (:anho) RETURNING id_periodo";
            $stmtPeriodoInsert = $pdo->prepare($queryPeriodoInsert);
            $stmtPeriodoInsert->execute([':anho' => validarEntero($institucion['periodo'])]);
            $id_periodo = $stmtPeriodoInsert->fetchColumn();
        }

        // Insertar Departamento
        $queryDepartamento = "SELECT id_departamento FROM Departamento WHERE codigo_departamento = :codigo_departamento";
        $stmtDepartamento = $pdo->prepare($queryDepartamento);
        $stmtDepartamento->execute([':codigo_departamento' => validarEntero($institucion['codigo_departamento'])]);
        $id_departamento = $stmtDepartamento->fetchColumn();

        if (!$id_departamento) {
            $queryDepartamentoInsert = "INSERT INTO Departamento (codigo_departamento, nombre_departamento) VALUES (:codigo_departamento, :nombre_departamento) RETURNING id_departamento";
            $stmtDepartamentoInsert = $pdo->prepare($queryDepartamentoInsert);
            $stmtDepartamentoInsert->execute([
                ':codigo_departamento' => validarEntero($institucion['codigo_departamento']),
                ':nombre_departamento' => $institucion['nombre_departamento']
            ]);
            $id_departamento = $stmtDepartamentoInsert->fetchColumn();
        }

        // Insertar Distrito
        $queryDistrito = "SELECT id_distrito FROM Distrito WHERE codigo_distrito = :codigo_distrito";
        $stmtDistrito = $pdo->prepare($queryDistrito);
        $stmtDistrito->execute([':codigo_distrito' => validarEntero($institucion['codigo_distrito'])]);
        $id_distrito = $stmtDistrito->fetchColumn();

        if (!$id_distrito) {
            $queryDistritoInsert = "INSERT INTO Distrito (codigo_distrito, nombre_distrito, id_departamento) VALUES (:codigo_distrito, :nombre_distrito, :id_departamento) RETURNING id_distrito";
            $stmtDistritoInsert = $pdo->prepare($queryDistritoInsert);
            $stmtDistritoInsert->execute([
                ':codigo_distrito' => validarEntero($institucion['codigo_distrito']),
                ':nombre_distrito' => $institucion['nombre_distrito'],
                ':id_departamento' => $id_departamento
            ]);
            $id_distrito = $stmtDistritoInsert->fetchColumn();
        }

        // Insertar Localidad
        // Si 'codigo_barrio_localidad' es nulo o vacío, usar 'codigo_distrito' y 'nombre_distrito'
        if (empty($institucion['codigo_barrio_localidad'])) {
            $codigo_barrio_localidad = validarEntero($institucion['codigo_distrito']);
            $nombre_barrio_localidad = $institucion['nombre_distrito'];
        } else {
            $codigo_barrio_localidad = validarEntero($institucion['codigo_barrio_localidad']);
            $nombre_barrio_localidad = $institucion['nombre_barrio_localidad'];
        }

        $queryLocalidad = "SELECT id_localidad FROM Localidad WHERE codigo_barrio_localidad = :codigo_barrio_localidad";
        $stmtLocalidad = $pdo->prepare($queryLocalidad);
        $stmtLocalidad->execute([':codigo_barrio_localidad' => $codigo_barrio_localidad]);
        $id_localidad = $stmtLocalidad->fetchColumn();

        if (!$id_localidad) {
            $queryLocalidadInsert = "INSERT INTO Localidad (codigo_barrio_localidad, nombre_barrio_localidad, id_distrito) 
                                     VALUES (:codigo_barrio_localidad, :nombre_barrio_localidad, :id_distrito) RETURNING id_localidad";
            $stmtLocalidadInsert = $pdo->prepare($queryLocalidadInsert);
            $stmtLocalidadInsert->execute([
                ':codigo_barrio_localidad' => $codigo_barrio_localidad,
                ':nombre_barrio_localidad' => $nombre_barrio_localidad,
                ':id_distrito' => $id_distrito
            ]);
            $id_localidad = $stmtLocalidadInsert->fetchColumn();
        }

        // Insertar Zona
        // Verificar si el código de la zona no es 1 o 2, si es así, asignar 1 por defecto
        if ($institucion['codigo_zona'] != 1 && $institucion['codigo_zona'] != 2) {
            $institucion['codigo_zona'] = 1;
        }
        $queryZona = "SELECT id_zona FROM Zona WHERE codigo_zona = :codigo_zona AND id_localidad = :id_localidad";
        $stmtZona = $pdo->prepare($queryZona);
        $stmtZona->execute([
            ':codigo_zona' => validarEntero($institucion['codigo_zona']),
            ':id_localidad' => $id_localidad
        ]);
        $id_zona = $stmtZona->fetchColumn();

        if (!$id_zona) {
            $queryZonaInsert = "INSERT INTO Zona (codigo_zona, nombre_zona, id_localidad) VALUES (:codigo_zona, :nombre_zona, :id_localidad) RETURNING id_zona";
            $stmtZonaInsert = $pdo->prepare($queryZonaInsert);
            $stmtZonaInsert->execute([
                ':codigo_zona' => validarEntero($institucion['codigo_zona']),
                ':nombre_zona' => $institucion['nombre_zona'],
                ':id_localidad' => $id_localidad
            ]);
            $id_zona = $stmtZonaInsert->fetchColumn();
        }

        // Insertar Establecimiento
        $queryEstablecimiento = "SELECT id_establecimiento FROM Establecimiento WHERE codigo_establecimiento = :codigo_establecimiento";
        $stmtEstablecimiento = $pdo->prepare($queryEstablecimiento);
        $stmtEstablecimiento->execute([':codigo_establecimiento' => validarEntero($institucion['codigo_establecimiento'])]);
        $id_establecimiento = $stmtEstablecimiento->fetchColumn();

        if (!$id_establecimiento) {
            $queryEstablecimientoInsert = "INSERT INTO Establecimiento (codigo_establecimiento, uri_establecimiento) VALUES (:codigo_establecimiento, :uri_establecimiento) RETURNING id_establecimiento";
            $stmtEstablecimientoInsert = $pdo->prepare($queryEstablecimientoInsert);
            $stmtEstablecimientoInsert->execute([
                ':codigo_establecimiento' => validarEntero($institucion['codigo_establecimiento']),
                ':uri_establecimiento' => $institucion['uri_establecimiento']
            ]);
            $id_establecimiento = $stmtEstablecimientoInsert->fetchColumn();
        }

        // Insertar Institución
        // Si 'codigo_institucion' es nulo o vacío, asignar 0 como código y "INSTITUCION NO REGISTRADA" como nombre
        $codigo_institucion = empty($institucion['codigo_institucion']) ? 0 : validarEntero($institucion['codigo_institucion']);
        $nombre_institucion = empty($institucion['nombre_institucion']) ? "INSTITUCION NO REGISTRADA" : $institucion['nombre_institucion'];

        $queryInstitucion = "SELECT id_institucion FROM Institucion WHERE codigo_institucion = :codigo_institucion";
        $stmtInstitucion = $pdo->prepare($queryInstitucion);
        $stmtInstitucion->execute([':codigo_institucion' => $codigo_institucion]);

        $id_institucion = $stmtInstitucion->fetchColumn();

        if (!$id_institucion) {
            $queryInstitucionInsert = "INSERT INTO Institucion (codigo_institucion, nombre_institucion, uri_institucion) 
                                       VALUES (:codigo_institucion, :nombre_institucion, :uri_institucion) RETURNING id_institucion";
            $stmtInstitucionInsert = $pdo->prepare($queryInstitucionInsert);
            $stmtInstitucionInsert->execute([
                ':codigo_institucion' => $codigo_institucion,
                ':nombre_institucion' => $nombre_institucion,
                ':uri_institucion' => $institucion['uri_institucion']
            ]);
            $id_institucion = $stmtInstitucionInsert->fetchColumn();
        }

        // Insertar Directorio
        $queryDirectorio = "SELECT id_directorio FROM Directorio WHERE directorio_institucion_id = :directorio_institucion_id";
        $stmtDirectorio = $pdo->prepare($queryDirectorio);
        $stmtDirectorio->execute([':directorio_institucion_id' => validarEntero($institucion['directorio_institucion_id'])]);
        $id_directorio = $stmtDirectorio->fetchColumn();

        if (!$id_directorio) {
            $queryDirectorioInsert = "INSERT INTO Directorio (directorio_institucion_id, id_periodo, id_establecimiento, id_institucion, id_zona) 
                                      VALUES (:directorio_institucion_id, :id_periodo, :id_establecimiento, :id_institucion, :id_zona)";
            $stmtDirectorioInsert = $pdo->prepare($queryDirectorioInsert);
            $stmtDirectorioInsert->execute([
                ':directorio_institucion_id' => validarEntero($institucion['directorio_institucion_id']),
                ':id_periodo' => $id_periodo,
                ':id_establecimiento' => $id_establecimiento,
                ':id_institucion' => $id_institucion,
                ':id_zona' => $id_zona
            ]);
        }

    } catch (Exception $e) {
        echo "Error al insertar: " . $e->getMessage();
    }
}

echo "Inserción masiva completada exitosamente.";

?>
