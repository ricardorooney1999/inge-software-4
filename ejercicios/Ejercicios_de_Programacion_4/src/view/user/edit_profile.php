<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Editar Perfil</title>
    <link rel="stylesheet" href="/css/estilos.css">
</head>
<body>
<div class="login-container">
<h1>Editar Perfil de <?php echo htmlspecialchars($user['nombre']); ?></h1>

<form method="POST" action="/user/edit">
    <!-- Incluye un campo oculto para el ID del usuario -->
    <input type="hidden" name="id" value="<?php echo htmlspecialchars($user['id']); ?>">

    <label for="nombre">Nombre:</label>
    <input type="text" name="nombre" id="nombre" value="<?php echo htmlspecialchars($user['nombre']); ?>" required>
    
    <br>

    <label for="apellido">Apellido:</label>
    <input type="text" name="apellido" id="apellido" value="<?php echo htmlspecialchars($user['apellido']); ?>" required>
    
    <br>

    <!-- Nuevo campo para el email -->
    <label for="email">Email:</label>
    <input type="email" name="email" id="email" value="<?php echo htmlspecialchars($user['email']); ?>" required>
    
    <br>

    <label for="password">Nueva Contraseña (dejar en blanco para no cambiarla):</label>
    <input type="password" name="password" id="password">

    <br>
    
    <button type="submit">Guardar Cambios</button>
    <a href="/user/profile" class="button-cancel">Cancelar</a>
</form>

</body>
</html>
