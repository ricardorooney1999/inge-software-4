<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil de Usuario</title>
    <link rel="stylesheet" href="/css/perfil.css">
</head>
<body>
    <div class="profile-container">
        <h1>Perfil de <?php echo htmlspecialchars($user['nombre']); ?></h1>
        <p><strong>Nombre:</strong> <?php echo htmlspecialchars($user['nombre']); ?></p>
        <p><strong>Apellido:</strong> <?php echo htmlspecialchars($user['apellido']); ?></p>
        <p><strong>Email:</strong> <?php echo htmlspecialchars($user['email']); ?></p>
        
        <!-- Opciones adicionales solo para el administrador -->
        <?php if (isset($_SESSION['role']) && $_SESSION['role'] === 'admin'): ?>
            <a href="/users_list" class="button">Listar Usuarios</a>
            <a href="/migrate_data" class="button">Migrar Datos</a>
        <?php endif; ?>
        
        <a href="/institutions_list" class="button">Listar Instituciones</a>
        <a href="/logout" class="button button-cancel">Deslogearse</a>
    </div>
</body>
</html>
