<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Lista de Usuarios</title>
    <link rel="stylesheet" href="/css/listUser.css">
</head>
<body>

    <h1>Lista de Usuarios</h1>

    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Editar</th>
            </tr>
        </thead>
        
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo htmlspecialchars($user['nombre']); ?></td>
                    <td><?php echo htmlspecialchars($user['apellido']); ?></td>
                    <td><?php echo htmlspecialchars($user['email']); ?></td>
                    <td>
                    <a href="/user/edit?id=<?php echo $user['id']; ?>">Editar</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <a href="/user/profile" class="button-cancel">Cancelar</a>

</body>
</html>
