<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de usuarios</title>
    <link rel="stylesheet" href="/css/estilos.css">
</head>
<body>
<div class="register-container">
        <h1>Registro de usuarios</h1>
        <form action="/register" method="POST">
            <label for="email">Correo electrónico:</label>
            <input type="email" name="email" required>
            
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" required>
            
            <label for="apellido">Apellido:</label>
            <input type="text" name="apellido" required> 
            
            <label for="password">Contraseña:</label>
            <input type="password" name="password" required>
            
            <button type="submit">Registrar</button>
            <a href="/" class="button-cancel">Cancelar</a>


        </form>
    </div>
</body>
</html>


