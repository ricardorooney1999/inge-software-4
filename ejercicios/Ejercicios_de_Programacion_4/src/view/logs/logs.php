<!DOCTYPE html>
<html>
<head>
    <title>Logs de Actividad</title>
    <link rel="stylesheet" href="/css/estilos.css">
</head>
<body>
    <h1>Logs de Actividad</h1>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Usuario</th>
            <th>Acción</th>
            <th>Descripción</th>
            <th>Fecha y Hora</th>
        </tr>
        <?php foreach ($logs as $log): ?>
            <tr>
                <td><?php echo htmlspecialchars($log['id']); ?></td>
                <td><?php echo htmlspecialchars($log['user_id']); ?></td>
                <td><?php echo htmlspecialchars($log['accion']); ?></td>
                <td><?php echo htmlspecialchars($log['descripcion']); ?></td>
                <td><?php echo htmlspecialchars($log['fecha_hora']); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
    <a href="/user/profile">Volver al Perfil</a>
</body>
</html>
