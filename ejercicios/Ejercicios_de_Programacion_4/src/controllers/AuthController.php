<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . '/../config/database.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../models/Log.php';

class AuthController
{
    private $userModel;
    private $logModel;
    private $pdo;


    public function __construct($pdo)
    {
        $this->userModel = new User($pdo);
        $this->logModel = new Log($pdo);
        $this->pdo = $pdo;
    }

    


    public function listUsers()
    {
        $users = $this->userModel->getAllUsers(); // Obtener todos los usuarios
        include __DIR__ . '/../view/auth/listUser.php'; // Mostrar la vista con los usuarios
    }

    public function showRegisterForm()
    {
        include __DIR__ . '/../view/auth/register.php';
    }

    public function register()
    {
        // Validar los datos enviados
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $email = $_POST['email'];
        $password = $_POST['password'];
    
        // Verificar si el email ya está registrado
        if ($this->userModel->findByEmail($email)) {
            $error = 'El correo electrónico ya está registrado.';
            
            include __DIR__ . '/../view/auth/register.php';
            return;
        }
    
        // Hashear la contraseña
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
    
        // Crear el array de datos del usuario
        $userData = [
            'nombre' => $nombre,
            'apellido' => $apellido,
            'email' => $email,
            'password' => $hashedPassword
        ];
    
        // Crear el usuario
        $this->userModel->createUser($userData);
    
        // Redirigir al login con un mensaje de éxito
        header('Location: /login?registered=1');
        exit;
    }

    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $this->userModel->findByEmail($email);

            if ($user && password_verify($password, $user['password'])) {
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['role'] = $user['role']; // Guardar el rol en la sesión
            
            // Verificar el rol y cambiar las credenciales de conexión
            if ($user['role'] === 'admin') {
                $this->connectAsAdmin();
            } else {
                $this->connectAsUser();
            }

                // Registrar log de ingreso
                $this->logModel->create([
                    'user_id' => $user['id'],
                    'accion' => 'login',
                    'descripcion' => 'Usuario inició sesión',
                    'fecha_hora' => date('Y-m-d H:i:s')
                ]);

                header('Location: /user/profile');
                exit();
            } else {
                $error = 'Usuario o contrasenia incorrecta';
            }
        }
        include __DIR__ . '/../view/auth/login.php';
    }

    public function connectAsAdmin()
    {
        try {
            $host = $_ENV['DB_HOST'];
            $dbname = $_ENV['DB_NAME'];
            $user = $_ENV['DB_USER'];  // Credenciales del rol de administrador
            $password = $_ENV['DB_PASSWORD'];

            $dsn = "pgsql:host=$host;dbname=$dbname";
            $pdo = new PDO($dsn, $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Guardar la conexión en una propiedad
            $this->pdo = $pdo;
        } catch (PDOException $e) {
            die('Error de conexión como admin: ' . $e->getMessage());
        }
    }


    public function connectAsUser()
    {
        try {
            $host = $_ENV['DB_HOST'];
            $dbname = $_ENV['DB_NAME'];
            $user = $_ENV['DB_USER'];  // Credenciales del rol de usuario
            $password = $_ENV['DB_PASSWORD'];

            $dsn = "pgsql:host=$host;dbname=$dbname";
            $pdo = new PDO($dsn, $user, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Guardar la conexión en una propiedad
            $this->pdo = $pdo;
        } catch (PDOException $e) {
            die('Error de conexión como usuario: ' . $e->getMessage());
        }
    }



    public function logout()
    {
        // Registrar log de salida
        if (isset($_SESSION['user_id'])) {
            $this->logModel->create([
                'user_id' => $_SESSION['user_id'],
                'accion' => 'logout',
                'descripcion' => 'Usuario cerró sesión',
                'fecha_hora' => date('Y-m-d H:i:s')
            ]);
        }

        session_destroy();
        header('Location: /login');
        exit();
    }




}
