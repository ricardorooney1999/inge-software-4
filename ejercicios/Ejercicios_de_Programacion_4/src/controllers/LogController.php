<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . '/../config/database.php';
require_once __DIR__ . '/../models/Log.php';

class LogController
{
    private $logModel;

    public function __construct($pdo)
    {
        $this->logModel = new Log($pdo);
    }

    public function index()
    {
        if (!isset($_SESSION['user_id'])) {
            header('Location: /login');
            exit();
        }

        $logs = $this->logModel->getAll();
        include __DIR__ . '/../view/logs/logs.php';
    }
}
