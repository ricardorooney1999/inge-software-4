<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . '/../config/database.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../models/Log.php';

class UserController
{
    private $userModel;
    private $logModel;
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo; 
        $this->userModel = new User($pdo);
        $this->logModel = new Log($pdo);
    }

    public function update($id, $data)
    {
        // Comienza a construir la consulta SQL
        $query = 'UPDATE users SET nombre = :nombre, apellido = :apellido, email = :email';
        
        // Solo incluye la contraseña si está en los datos
        if (isset($data['password'])) {
            $query .= ', password = :password';
        }
    
        // Termina la consulta
        $query .= ' WHERE id = :id';
    
        // Prepara la declaración SQL
        $stmt = $this->pdo->prepare($query);
    
        // Asigna los valores de nombre, apellido, email y si está disponible, la contraseña
        $stmt->bindParam(':nombre', $data['nombre']);
        $stmt->bindParam(':apellido', $data['apellido']);
        $stmt->bindParam(':email', $data['email']);
    
        if (isset($data['password'])) {
            $stmt->bindParam(':password', $data['password']);
        }
    
        // Asegúrate de que el ID se pasa correctamente
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    
        // Ejecuta la actualización
        return $stmt->execute();
    }
    


    public function profile()
    {
        if (!isset($_SESSION['user_id'])) {
            header('Location: /login');
            exit();
        }

        $user = $this->userModel->findById($_SESSION['user_id']);
        include __DIR__ . '/../view/user/profile.php';
        //include __DIR__ . '/../../muestraDeDatos.php';
    }


    public function deleteUser()
    {
        if (!isset($_SESSION['user_id'])) {
            header('Location: /login');
            exit();
        }

        $userId = $_SESSION['user_id'];

        // Llamamos al método delete del modelo User
        $this->userModel->delete($userId);

        // Registrar log de eliminación
        $this->logModel->create([
            'user_id' => $userId,
            'accion' => 'delete',
            'descripcion' => 'Usuario marcado como inactivo',
            'fecha_hora' => date('Y-m-d H:i:s')
        ]);

        // Cerramos la sesión ya que el usuario se eliminó
        session_destroy();
        header('Location: /login');
        exit();
    }

    public function editProfile()
    {

        echo "Revisando si se ha enviado el formulario...<br>"; // Este debería imprimirse siempre

        // Verifica si el formulario fue enviado
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            echo "Formulario enviado correctamente como POST<br>";
            $nombre = $_POST['nombre'];
            $apellido = $_POST['apellido'];
            $email = $_POST['email'];  // Nuevo campo email
            $password = $_POST['password']; // Esto podría estar vacío si el usuario no quiere cambiar la contraseña
            $userId = $_POST['id'] ?? $_GET['id']; 

            // Debug: Verifica los valores antes de la actualización
            echo "ID del usuario: $userId <br>";
            echo "Nombre: $nombre <br>";
            echo "Apellido: $apellido <br>";
            echo "Email: $email <br>";
            echo "Password: " . (!empty($password) ? "proporcionado" : "no proporcionado") . "<br>";
    
            // Obtén el ID del usuario que se está editando (del formulario o de la URL)
            $userId = $_POST['id'] ?? $_GET['id']; 
    
            // Crea un array de datos para actualizar (sin la contraseña aún)
            $userData = [
                'nombre' => $nombre,
                'apellido' => $apellido,
                'email' => $email // Incluye el email en los datos a actualizar
            ];
    
            // Si el campo de la contraseña no está vacío, actualiza la contraseña
            if (!empty($password)) {
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
                $userData['password'] = $hashedPassword;
            }
    
            // Llama al método en el modelo para actualizar el usuario
            $updated = $this->userModel->update($userId, $userData);
    
            // Verifica si la actualización fue exitosa
            if ($updated) {
                // Redirige al perfil del usuario con un mensaje de éxito
                header('Location: /user/profile?updated=1');
                exit();
            } else {
                // Si la actualización falló, muestra un mensaje de error
                echo "Error al actualizar el perfil.";
            }
        }
    
        // Si el método no es POST, muestra el formulario de edición
        $userId = $_GET['id'] ?? $_SESSION['user_id']; // Carga el usuario basado en el id de la URL o del logueado
        $user = $this->userModel->findById($userId);
        include __DIR__ . '/../view/user/edit_profile.php';

        // Debug: Verifica los valores antes de la actualización

    }
    
}
