-- Database: bdEjercitario4

-- DROP DATABASE IF EXISTS "bdEjercitario4";

CREATE DATABASE "bdEjercitario4"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    LOCALE_PROVIDER = 'libc'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

GRANT TEMPORARY, CONNECT ON DATABASE "bdEjercitario4" TO PUBLIC;

GRANT ALL ON DATABASE "bdEjercitario4" TO admin;

GRANT ALL ON DATABASE "bdEjercitario4" TO postgres;


-- Crear tabla 'users' en la base de datos 'bdEjercitario4'
-- Table: public.logs

-- DROP TABLE IF EXISTS public.logs;

CREATE TABLE IF NOT EXISTS public.logs
(
    id integer NOT NULL DEFAULT nextval('logs_id_seq'::regclass),
    user_id integer NOT NULL,
    accion character varying(50) COLLATE pg_catalog."default",
    descripcion text COLLATE pg_catalog."default",
    fecha_hora timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT logs_pkey PRIMARY KEY (id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.logs
    OWNER to postgres;

-- Crear tabla 'logs' en la base de datos 'bdEjercitario4'
-- Table: public.users

-- DROP TABLE IF EXISTS public.users;

CREATE TABLE IF NOT EXISTS public.users
(
    id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    nombre character varying(50) COLLATE pg_catalog."default" NOT NULL,
    apellido character varying(50) COLLATE pg_catalog."default" NOT NULL,
    email character varying(100) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default" NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    activo boolean DEFAULT true,
    role character varying(20) COLLATE pg_catalog."default" DEFAULT 'user',
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT users_email_key UNIQUE (email)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.users
    OWNER to postgres;

GRANT ALL ON TABLE public.users TO admin;

GRANT ALL ON TABLE public.users TO postgres;


///////////////////////////////

-- Asignar permisos a soloConsultas
GRANT SELECT ON TABLE users TO soloConsultas;
GRANT SELECT ON TABLE logs TO soloConsultas;
GRANT SELECT ON SEQUENCE users_id_seq TO soloConsultas;
GRANT SELECT ON SEQUENCE logs_id_seq TO soloConsultas;

-- Asignar permisos a soloEditores
GRANT SELECT, INSERT, UPDATE ON TABLE users TO soloEditores;
GRANT SELECT, INSERT, UPDATE ON TABLE logs TO soloEditores;
GRANT USAGE, SELECT ON SEQUENCE users_id_seq TO soloEditores;
GRANT USAGE, SELECT ON SEQUENCE logs_id_seq TO soloEditores;

/*
-- Crear usuarios y asignar roles
CREATE USER usuario_consulta WITH PASSWORD 'contraseña_segura';
GRANT soloConsultas TO usuario_consulta;

CREATE USER usuario_editor WITH PASSWORD 'contraseña_segura';
GRANT soloEditores TO usuario_editor;
*/
CREATE TABLE Periodo (
    id_periodo SERIAL PRIMARY KEY,
    anho INTEGER NOT NULL CHECK (anho >= 1900 AND anho <= EXTRACT(YEAR FROM CURRENT_DATE))
);

-- Creación de la tabla Departamento
CREATE TABLE Departamento (
    id_departamento SERIAL PRIMARY KEY,
    codigo_departamento CHAR(2) NOT NULL,
    nombre_departamento VARCHAR(40) NOT NULL
);

-- Creación de la tabla Distrito
CREATE TABLE Distrito (
    id_distrito SERIAL PRIMARY KEY,
    codigo_distrito CHAR(2) NOT NULL,
    nombre_distrito VARCHAR(40) NOT NULL,
    id_departamento INTEGER REFERENCES Departamento(id_departamento)
);

-- Creación de la tabla Localidad
CREATE TABLE Localidad (
    id_localidad SERIAL PRIMARY KEY,
    codigo_barrio_localidad INTEGER NOT NULL,
    nombre_barrio_localidad VARCHAR(40) NOT NULL,
    id_distrito INTEGER REFERENCES Distrito(id_distrito)
);

-- Creación de la tabla Zona
CREATE TABLE Zona (
    id_zona SERIAL PRIMARY KEY,
    codigo_zona CHAR(1) NOT NULL CHECK (codigo_zona IN ('1', '2')),
    nombre_zona VARCHAR(7) NOT NULL,
    id_localidad INTEGER REFERENCES Localidad(id_localidad)
);

-- Creación de la tabla Establecimiento
CREATE TABLE Establecimiento (
    id_establecimiento SERIAL PRIMARY KEY,
    codigo_establecimiento CHAR(7) NOT NULL,
    uri_establecimiento VARCHAR(255) NOT NULL
);

-- Creación de la tabla Institucion
CREATE TABLE Institucion (
    id_institucion SERIAL PRIMARY KEY,
    codigo_institucion CHAR(5) NOT NULL,
    nombre_institucion VARCHAR(40) NOT NULL,
    uri_institucion VARCHAR(255) NOT NULL
);

-- Creación de la tabla Directorio
CREATE TABLE Directorio (
    id_directorio SERIAL PRIMARY KEY,
    directorio_institucion_id INTEGER NOT NULL UNIQUE,
    id_periodo INTEGER REFERENCES Periodo(id_periodo),
    id_establecimiento INTEGER REFERENCES Establecimiento(id_establecimiento),
    id_institucion INTEGER REFERENCES Institucion(id_institucion),
    id_zona INTEGER REFERENCES Zona(id_zona)
);

-- Agregar restricciones adicionales para claves únicas (si es necesario)
ALTER TABLE Establecimiento ADD CONSTRAINT unique_codigo_establecimiento UNIQUE (codigo_establecimiento);
ALTER TABLE Institucion ADD CONSTRAINT unique_codigo_institucion UNIQUE (codigo_institucion);

-- Índices para mejorar rendimiento de búsqueda
CREATE INDEX idx_establecimiento_codigo ON Establecimiento(codigo_establecimiento);
CREATE INDEX idx_institucion_codigo ON Institucion(codigo_institucion);
CREATE INDEX idx_directorio_institucion_id ON Directorio(directorio_institucion_id);