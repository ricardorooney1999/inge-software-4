-- Crear los roles/grupos de permisos si no existen

------------------------------------------------------------------------------------
----------USAR PROYECTO PARA CARGAR USUARIOS, admin@admin.com pass : admin----------
------------------------------------------------------------------------------------



DO
$$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = 'soloConsultas') THEN
        CREATE ROLE soloConsultas;
    END IF;

    IF NOT EXISTS (SELECT 1 FROM pg_roles WHERE rolname = 'soloEditores') THEN
        CREATE ROLE soloEditores;
    END IF;
END
$$;

-- Crear usuarios y asignarles roles
CREATE USER usuarioView WITH PASSWORD '12345';
GRANT soloConsultas TO usuarioView;

CREATE USER usuarioEditor WITH PASSWORD '12345';
GRANT soloEditores TO usuarioEditor;

-- Asignar permisos a soloConsultas
GRANT SELECT ON TABLE users TO soloConsultas;
GRANT SELECT ON TABLE logs TO soloConsultas;
GRANT SELECT ON SEQUENCE users_id_seq TO soloConsultas;
GRANT SELECT ON SEQUENCE logs_id_seq TO soloConsultas;

-- Asignar permisos a soloEditores
GRANT SELECT, INSERT, UPDATE ON TABLE users TO soloEditores;
GRANT SELECT, INSERT, UPDATE ON TABLE logs TO soloEditores;
GRANT USAGE, SELECT ON SEQUENCE users_id_seq TO soloEditores;
GRANT USAGE, SELECT ON SEQUENCE logs_id_seq TO soloEditores;
