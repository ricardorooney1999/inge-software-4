<?php
require_once __DIR__ . '/../../vendor/autoload.php';

//use PDO;
//use PDOException;
use Dotenv\Dotenv; // Añade esta línea

$dotenv = Dotenv::createImmutable(__DIR__ . '/../../');
$dotenv->load();

// lo que esta en el archivo
$host = $_ENV['DB_HOST'];
$port = $_ENV['DB_PORT'];
$dbname = $_ENV['DB_NAME'];
$user = $_ENV['DB_USER'];
$password = $_ENV['DB_PASSWORD'];

try {
    $dsn = "pgsql:host=$host;port=$port;dbname=$dbname;";
    $pdo = new PDO($dsn, $user, $password);
    // Configurar el manejo de errores de PDO
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Error de conexión: ' . $e->getMessage());
}
return $pdo;
