<?php

class Log
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function create($data)
    {
        $stmt = $this->pdo->prepare('INSERT INTO logs (user_id, accion, descripcion, fecha_hora) VALUES (:user_id, :accion, :descripcion, :fecha_hora)');
        return $stmt->execute($data);
    }

    public function getAll()
    {
        $stmt = $this->pdo->query('SELECT * FROM logs ORDER BY fecha_hora DESC');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
