<?php

class User
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function findByEmail($email)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE email = :email AND activo = TRUE');
        $stmt->execute(['email' => $email]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
   

    public function createUser($data)
    {
        $stmt = $this->pdo->prepare('INSERT INTO users (nombre, apellido, email, password) VALUES (:nombre, :apellido, :email, :password)');
        return $stmt->execute($data);
    }

    public function create($email, $password)
    {
        $stmt = $this->pdo->prepare('INSERT INTO users (email, password, activo) VALUES (:email, :password, TRUE)');
        $stmt->execute(['email' => $email, 'password' => $password]);
    }

    public function update($id, $data)
    {
        // Comienza a construir la consulta SQL
        $query = 'UPDATE users SET nombre = :nombre, apellido = :apellido, email = :email';
        
        // Solo incluye la contraseña si está en los datos
        if (isset($data['password'])) {
            $query .= ', password = :password';
        }
    
        // Termina la consulta
        $query .= ' WHERE id = :id';
    
        // Prepara la declaración SQL
        $stmt = $this->pdo->prepare($query);
    
        // Asigna los valores de nombre, apellido, email y si está disponible, la contraseña
        $stmt->bindParam(':nombre', $data['nombre']);
        $stmt->bindParam(':apellido', $data['apellido']);
        $stmt->bindParam(':email', $data['email']);
    
        if (isset($data['password'])) {
            $stmt->bindParam(':password', $data['password']);
        }
    
        // Asegúrate de que el ID se pasa correctamente
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    
        // Ejecuta la actualización
        return $stmt->execute();
    }
    
    

    public function findById($id)
    {
        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE id = :id AND activo = TRUE');
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function delete($id)
    {
        $stmt = $this->pdo->prepare('UPDATE users SET activo = FALSE WHERE id = :id');
        return $stmt->execute(['id' => $id]);
    }

    public function getAllUsers()
    {
    $stmt = $this->pdo->prepare('SELECT * FROM users');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
}
