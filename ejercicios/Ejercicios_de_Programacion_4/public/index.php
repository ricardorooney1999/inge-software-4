<?php

require_once __DIR__ . '/../vendor/autoload.php';
$pdo = require __DIR__ . '/../src/config/database.php'; // Captura el valor devuelto de database.php

require_once __DIR__ . '/../src/controllers/AuthController.php';
require_once __DIR__ . '/../src/controllers/UserController.php';
require_once __DIR__ . '/../src/controllers/LogController.php';
// Crea instancias de los controladores y pasa el objeto $pdo
$authController = new AuthController($pdo);
$userController = new UserController($pdo);
$logController = new LogController($pdo);

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

switch ($uri) {
    case '':
    case '/':
        $authController->login();
        break;
    case '/login':
        $authController->login();
        break;
    case '/logout':
        $authController->logout();
        break;
    case '/user/profile':
        $userController->profile();
        break;
    case '/register':
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $authController->showRegisterForm();
        } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $authController->register();
        }
        break;
        case '/user/edit':
            if (isset($_GET['id'])) {
                $userController->editProfile(); 
            } else {
                // En caso de que no haya ID, redirigir a la lista de usuarios o mostrar un error.
                header('Location: /users_list');
            }
            break;
    case '/logs':
        $logController->index();
        break;
    case '/user/delete':
        $userController->deleteUser();
        break; 
    case '/users_list':
        $authController->listUsers();
        break;
    case '/institutions_list':
        include __DIR__ . '/../muestraDeDatos.php';
        break;
    case "/migrate_data" :
        include __DIR__ . '/../scrip_migracion.php';
    default:
        header('HTTP/1.0 404 Not Found');
        echo 'Página no encontrada';
        break;
}
