@section('editarProveedor')

<div class="container mt-5">
    <h2>Editar Proveedor</h2>
    <form action="{{ route('proveedor.update', ['id' => $proveedor->ID_PROVEEDOR]) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="mb-3">
            <label for="ID_PROVEEDOR" class="form-label">ID Proveedor</label>
            <input type="text" class="form-control" id="ID_PROVEEDOR" name="ID_PROVEEDOR" value="{{ $proveedor->ID_PROVEEDOR }}" readonly>
        </div>

        <div class="mb-3">
            <label for="ID_CATEGORIA" class="form-label">ID Categoría</label>
            <input type="number" class="form-control" id="ID_CATEGORIA" name="ID_CATEGORIA" value="{{ $proveedor->ID_CATEGORIA }}">
        </div>

        <div class="mb-3">
            <label for="NOMBRE" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="NOMBRE" name="NOMBRE" value="{{ $proveedor->NOMBRE }}" required>
        </div>

        <div class="mb-3">
            <label for="RUC" class="form-label">RUC</label>
            <input type="text" class="form-control" id="RUC" name="RUC" value="{{ $proveedor->RUC }}" required>
        </div>

        <div class="mb-3">
            <label for="CONTACTO" class="form-label">Contacto</label>
            <input type="text" class="form-control" id="CONTACTO" name="CONTACTO" value="{{ $proveedor->CONTACTO }}">
        </div>

        <div class="mb-3">
            <label for="EMAIL" class="form-label">Email</label>
            <input type="email" class="form-control" id="EMAIL" name="EMAIL" value="{{ $proveedor->EMAIL }}">
        </div>

        <div class="mb-3">
            <label for="TELEFONO" class="form-label">Teléfono</label>
            <input type="text" class="form-control" id="TELEFONO" name="TELEFONO" value="{{ $proveedor->TELEFONO }}">
        </div>

        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        <a href="{{ route('proveedor.index') }}" class="btn btn-secondary">Cancelar</a>
    </form>
</div>

@endsection
