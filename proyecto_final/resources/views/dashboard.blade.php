<!--<x-app-layout>-->
@extends('layouts.app_principal')
@section('title', 'Panel Principal')
@section('panelPrincipal')
        <div class="sidebar">

            <div class="head">
                <div class="user-img">
                    <img src="{{ asset('img/usuario.png') }}" alt="Logo del login">
                </div>
                <div class="user-details">
                    <p class="title">Web developer</p>                        
                    <p class="name">Pedro Baez</p>
                </div>
            </div>
            <div class="nav">
                <div class="menu">
                    <p class="title">Main</p>
                    <ul>
                        <li>
                            <a href="#">
                                <!--<i class="icon ph-bold ph-house-simple"></i> 
                                <span class="text">Home</span>
                                <span class="material-icons-outlined">home</span>
                                <span class="text">Home</span>-->

                                <i class="bi bi-1-square"></i>
                                <span class="text">Home</span>

                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon ph-bold ph-user"></i> 
                                <span class="text">Ordenes de compras</span>
                                <i class="arrow ph-bold ph-caret-down"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#">
                                        <span class="text">prueba subMenu11</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="text">prueba subMenu22</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class = "active">
                            <a href="#">
                                <i class="icon ph-bold ph-file-text"></i> 
                                <span class="text">Proveedores</span>
                            </a>
                        </li>
                        <li class = "active">
                            <a href="#">
                            <span class="material-icons-outlined"></span>
                            <span class="text">Reportes</span>
                            </a>
                        </li>
                    </ul>
                </div>               
            </div>
            <div class="menu">
                <p class="title">Cuenta</p>
                <ul>
                    <li>
                        <a href="#">
                            <i class = "icon ph-bold ph-info"></i>
                            <span class="text">Ayuda</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class = "icon ph-bold ph-sign-out"></i>
                            <span class="text">Cerrar Sesión</span>
                        </a>
                </ul>
            </div>
        </div>
@endsection

@section('tab')



<div class="container-fluid mt-4">
    <div class="row">
        <div class="col-12">
            <h2>Ordenes de compras</h2>
            <hr />
        </div>
    </div>

        <div class="row mb-3">
            <div class="col-md-3 text-start">
                <div class="input-group input-group-sm">
                    <input class="form-control py-1 rounded-pill mr-1 pr-3" type="search" placeholder="Buscar" id="example-search-input">
                    <span class="input-group-append">
                        <button class="btn rounded-pill border-0 ms-n4" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </div>


            <div class="col-auto d-flex align-items-center">
                <button class="btn btn-primary me-2">Buscar</button>
                <select class="form-select form-select-sm" aria-label=".form-select-sm example">
                    <option selected>Filtro</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                </select>
            </div>

    </div>

    <div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="true" href="#">Active</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <h5 class="card-title">Special title treatment</h5>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Categoría</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Aquí irán los datos de la tabla -->
                    <tr>
                        <td>1</td>
                        <td>Proveedor A</td>
                        <td>Categoría X</td>
                        <td>
                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                                <button class="btn btn-success">Editar</button>
                                <button class="btn btn-danger">Eliminar</button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
  </div>


</div>


</div>
@endsection
<!--</x-app-layout>-->
