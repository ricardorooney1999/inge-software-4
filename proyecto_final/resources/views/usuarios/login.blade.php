@extends('layouts.app')
@section('login')

<!----------------------- Main Container -------------------------->
<div class="container d-flex justify-content-center align-items-center min-vh-100">
    <!----------------------- Login Container -------------------------->
       <div class="row border rounded-5 p-3 bg-white shadow box-area">
    <!--------------------------- Left Box ----------------------------->
       <div class="col-md-6 rounded-4 d-flex justify-content-center align-items-center flex-column left-box" style="background: #103cbe;">
           <div class="featured-image mb-3">
           <img src="{{ asset('img/usuario.png') }}" alt="Logo del login" width="200" height="200">
           </div>
           <small class="text-white text-wrap text-center" style="width: 17rem;font-family: 'Courier New', Courier, monospace;">Frase del dia.</small>

           <p class="text-white fs-5" style="font-family: 'Courier New', Courier, monospace; font-weight: 400;">Frase, buscar en la red</p>
       </div> 
    <!-------------------- ------ Right Box ---------------------------->
        
       <div class="col-md-6 right-box">
          <div class="row align-items-center">
                <div class="header-text mb-4">
                     <h2>Bienvenido</h2>
                     <p>Comencemos y completa tus datos.</p>
                </div>
                <!-- Formulario de Login -->
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="text" class="form-control form-control-lg bg-light fs-6" placeholder="Usuario">
                    </div>
                    <div class="input-group mb-1">
                        <input type="password" class="form-control form-control-lg bg-light fs-6" placeholder="Conrtrasenia">
                    </div>
                    <div class="input-group mb-5 d-flex justify-content-between">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="formCheck">
                            <label for="formCheck" class="form-check-label text-secondary"><small>Recuerdame</small></label>
                        </div>
                        <div class="forgot">
                            <small><a href="{{ route('login.create') }}">olvide mi contrasenia?</a></small>
                        </div>
                    </div>
                    <!--
                    <div class="input-group mb-3">
                        <button class="btn btn-lg btn-primary w-100 fs-6">Login</button>
                    </div>
                    -->
                    <!--sin logica de autenticacion-->
                    <div class="input-group mb-3">
                        <a href="{{ url('/panelPrincipal') }}" class="btn btn-lg btn-primary w-100 fs-6">Login</a>
                    </div>
                </form>
                <div class="row">
                    <small>¿No tienes cuenta? <a href="{{ route('login.create') }}">Crear aquí</a></small>
                </div>
          </div>
       </div> 
      </div>
    </div>

@endsection
