<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- ICONS 
    <script src="https://unpkg.com/@phosphor-icons/web"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.js" integrity="sha512-8Z5++K1rB3U+USaLKG6oO8uWWBhdYsM3hmdirnOEWp8h2B1aOikj5zBzlXs8QOrvY9OxEnD2QDkbSKKpfqcIWw==" crossorigin="anonymous"></script>
    <title>@yield('title', 'Panel')</title>
    <!-- enlaces CSS-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/panelPrincipal.js') }}"></script>
</head>
<body>
    <header>
        <!-- Aquí podrías añadir un header si es necesario -->
    </header>

    <div class="wrapper">
        <div class="main panel">
            <!-- sección izquierda -->
            @yield('panelPrincipal')
        </div>

        <div class="main-content">
            <!-- sección derecha -->
            @yield('tab')
        </div>
    </div>

    <footer>
        <!--footer-->
    </footer>
</body>
</html>
