var docs = [
    {
        "Type": "excel",
        "Name": "Remaining tasks for this app",
        "Description": "This is a list of all the remaining tasks required to complete this app",
        "Tags": "Responsive, RWD",
        "LastViewed": "an hour ago",
        "Expiration": "Sep 17, 2015"
    },
    {
        "Type": "ppt",
        "Name": "EVAMs presentation",
        "Description": "This is a presentation for the EVAM occurring later this month",
        "Tags": "EVAM",
        "LastViewed": "a day ago",
        "Expiration": "Sep 08, 2015"
    },
    {
        "Type": "word",
        "Name": "Xmas Party list",
        "Description": "List of all the people who will be attending the holiday party",
        "Tags": "Responsive, RWD",
        "LastViewed": "a few mins ago",
        "Expiration": "Dec 25, 2014"
    }
];
