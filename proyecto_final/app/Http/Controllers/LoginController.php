<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; // Para manejar peticiones HTTP
use Illuminate\Contracts\View\Factory as ViewFactory; // Para manejar vistas
use Illuminate\Contracts\View\View; // Para manejar vistas

class LoginController extends Controller
{
    public function showLoginForm(): View
    {
        return view('usuarios.login');
    }

    public function showCreateForm(): View
    {
        return view('usuarios.create_login');
    }

    public function showEditForm(): View
    {
        return view('usuarios.edit_login');
    }

    public function showForgoPassword(): View
    {
        return view('usuarios.forgo_password');
    }

    public function showPanelPrincipal(): View
    {
        return view('panelPrincipal');
    }

    public function showAppPrincipal(): View
    {
        return view('app_principal');
    }



    // verificacion de logeo
    public function login(Request $request)
    {
        /*
        // Validar los datos del formulario
        $credentials = $request->validate([
            'username' => ['required', 'string'],
            'password' => ['required'],
        ]);

        // Intentar autenticar
        if (Auth::attempt($credentials, $request->filled('remember'))) {
            // Redirigir al panel principal si es exitoso
            return redirect()->intended('/panelPrincipal');
        }

        // Si falla, redirigir de nuevo con un mensaje de error
        return back()->withErrors([
            'username' => 'Las credenciales no coinciden.',
        ]);
        */
    }
}